package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public abstract class View {
    
    protected final IResource resource;

    public View(IResource resource) {
        this.resource = resource;
    }
    
    public abstract String render();
}
