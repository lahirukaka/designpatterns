package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public class ReptileFactory implements IFactory{

    @Override
    public Animal create() {
        /*Logic of creating reptile*/
        return new Snake();
    }
}
