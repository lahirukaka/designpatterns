package designpatterns.strategy_pattern;

/**
 *
 * @author lahiru
 */
public final class AccelerateBStrategy implements IAccelerateStrategy{

    @Override
    public void accelerate() {
        System.out.println("Starting B strategy to accelerate");
    }
    
    
}
