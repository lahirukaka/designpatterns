package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public interface IBeverage {

    int cost();
    String description();
}