package designpatterns.strategy_pattern;

/**
 *
 * @author lahiru
 */
public interface IAccelerateStrategy {
    
    void accelerate();
}
