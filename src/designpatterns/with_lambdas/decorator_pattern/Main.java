package designpatterns.with_lambdas.decorator_pattern;

import java.awt.Color;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        print(new Camera());
        print(new Camera(Color::brighter));
        print(new Camera(Color::darker));
        print(new Camera(Color::darker, Color::brighter));
    }
    
    public static void print(Camera camera)
    {
        System.out.println(camera.snap(new Color(128, 128, 128)));
    }
}
