package designpatterns.adapter_pattern;

/**
 *
 * @author lahiru
 */
public class Adapter implements IAdapter{

    private final Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }
    
    /*
    * This method convert one method signature to another
    */
    @Override
    public void call() {
        adaptee.someMethod();
    }
}
