package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public abstract class AddonFlavorsDecorator implements IBeverage{
    
    protected IBeverage beverage;
    
    @Override
    public abstract int cost();

    @Override
    public abstract String description();
}
