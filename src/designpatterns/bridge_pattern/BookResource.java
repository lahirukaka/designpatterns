package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public class BookResource implements IResource{

    private final Book book;

    public BookResource(Book book) {
        this.book = book;
    }

    @Override
    public String snippet() {
        return book.getCover_text();
    }

    @Override
    public String title() {
        return book.getName();
    }
}
