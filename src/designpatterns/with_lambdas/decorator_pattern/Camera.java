package designpatterns.with_lambdas.decorator_pattern;

import java.awt.Color;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 *
 * @author lahiru
 */
public class Camera {
    
    private final Function<Color, Color> filter;

    public Camera(Function<Color, Color>... filters) {
        filter = Stream.of(filters)
                .reduce(Function.identity(), Function::andThen);
    }
    
    public Color snap(Color input)
    {
        return filter.apply(input);
    }
}
