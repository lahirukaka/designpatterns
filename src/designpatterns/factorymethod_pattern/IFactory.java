package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public interface IFactory {

    Animal create();
}
