package designpatterns.factorymethod_pattern;

import java.util.Random;

/**
 *
 * @author lahiru
 */
public class MammalFactory implements IFactory{

    @Override
    public Animal create() {
        Random rand = new Random();
        int number = rand.nextInt(2)+1;
        if(number==1){return new Cat();}
        else return new Dog();
    }
}
