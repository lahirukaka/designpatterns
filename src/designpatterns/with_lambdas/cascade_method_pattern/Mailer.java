package designpatterns.with_lambdas.cascade_method_pattern;

import java.util.function.Consumer;

/**
 *
 * @author lahiru
 */
public class Mailer {

    private String from;
    private String to;
    private String body;
    private String subject;

    public static void send(Consumer<Mailer> instance) {
        Mailer mail = new Mailer();
        instance.accept(mail);
        System.out.println("sending mail : " + mail.toString());
        System.out.println("Done");
    }

    private Mailer() {
        System.out.println("Creating a mail...");
    }

    public Mailer setAddresses(String from, String to) {
        this.from = from;
        this.to = to;
        return this;
    }

    public Mailer setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public Mailer setBody(String body) {
        this.body = body;
        return this;
    }

    @Override
    public String toString() {
        return "Mailer{" + "from=" + from + ", to=" + to + ", body=" + body + ", subject=" + subject + '}';
    }
}
