package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public class Multiply implements ICommand{

    private final Receiver receiver;
    private final int number;

    public Multiply(Receiver receiver, int number) {
        this.receiver = receiver;
        this.number = number;
    }
    
    @Override
    public void execute() {
        this.receiver.multiplyByNumber(number);
    }
    
    @Override
    public void undo() {
        this.receiver.divideByNumber(number);
    }
}
