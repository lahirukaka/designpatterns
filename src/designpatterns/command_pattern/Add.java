package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public class Add implements ICommand{

    private final int number;
    private final Receiver receiver;

    public Add(int number, Receiver receiver) {
        this.number = number;
        this.receiver = receiver;
    }
    
    @Override
    public void execute() {
        receiver.addNumber(number);
    }

    @Override
    public void undo() {
        receiver.substractNumber(number);
    }
}
