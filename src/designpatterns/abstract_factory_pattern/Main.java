package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        //for Dark theme
        DarkThemeUIFactory darkTheme = new DarkThemeUIFactory();
        darkTheme.createButton().render();
        darkTheme.createTextView().render();
        
        //for Light theme
        LightThemeUIFactory lightTheme = new LightThemeUIFactory();
        lightTheme.createButton().render();
        lightTheme.createTextView().render();
    }
}
