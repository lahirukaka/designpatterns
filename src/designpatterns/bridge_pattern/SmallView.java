package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public class SmallView extends View{

    public SmallView(IResource resource) {
        super(resource);
    }

    @Override
    public String render() {
        return this.resource.title();
    }
}
