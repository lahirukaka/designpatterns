package designpatterns.strategy_pattern;

/**
 *
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        Vehicle vehicle1 = new Vehicle(new AccelerateAStrategy());
        Vehicle vehicle2 = new Vehicle(new AccelerateBStrategy());
        
        vehicle1.accelerate();
        vehicle2.accelerate();
    }
}
