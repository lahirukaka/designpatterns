package designpatterns.with_lambdas.execute_around_pattern;

import java.util.function.Consumer;

/**
 *
 * @author lahiru
 */
public class Resource {
    
    private Resource(){
        System.out.println("creating resource");
    }
    
    public Resource operation1()
    {
        System.out.println("operation 1");
        return this;
    }
    
    public Resource operation2()
    {
        System.out.println("operation 2");
        return this;
    }
    
    private void close()
    {
        System.out.println("cleaning out");
    }
    
    public static void use(Consumer<Resource> block)
    {
        final Resource resource = new Resource();
        try{
            block.accept(resource);
        }finally{
            resource.close();
        }
    }
}
