package designpatterns.bridge_pattern;

/**
 * 
 * @author lahiru
 */
public class Book {
    
    private final String name;
    private final String cover_text;

    public Book(String name, String cover_text) {
        this.name = name;
        this.cover_text = cover_text;
    }

    public String getName() {
        return name;
    }

    public String getCover_text() {
        return cover_text;
    }
}
