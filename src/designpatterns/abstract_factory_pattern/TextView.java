package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public abstract class TextView {
    
    public abstract void render();
}
