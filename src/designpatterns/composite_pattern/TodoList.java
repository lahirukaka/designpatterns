package designpatterns.composite_pattern;

import java.util.List;

/**
 * This is a list of Todos
 * @author lahiru
 */
public class TodoList implements IComponent{
    
    private final String text;
    private final List<IComponent> todos;

    public TodoList(String text, List<IComponent> todos) {
        this.text = text;
        this.todos = todos;
    }

    @Override
    public StringBuilder render() {
        StringBuilder html = new StringBuilder();
        html.append("\n<li>").append(text).append("</li>\n<ul>");
        todos.stream()
            .forEach(todo->html.append(todo.render()));
        html.append("\n</ul>");
        return html;
    }
}
