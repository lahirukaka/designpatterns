package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public class Divide implements ICommand{

    private final Receiver receiver;
    private final int number;

    public Divide(Receiver receiver, int number) {
        this.receiver = receiver;
        this.number = number;
    }
    
    @Override
    public void execute() {
        this.receiver.divideByNumber(number);
    }
    
    @Override
    public void undo() {
        this.receiver.multiplyByNumber(number);
    }
}
