package designpatterns.adapter_pattern;

/**
 *
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        IAdapter adapter = new Adapter(new Adaptee());
        adapter.call();
    }
}
