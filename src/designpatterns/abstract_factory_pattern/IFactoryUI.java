package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public interface IFactoryUI {
    Button createButton();
    TextView createTextView();
}
