package designpatterns.adapter_pattern;

/**
 *
 * @author lahiru
 */
public class Adaptee {
    
    public void someMethod()
    {
        System.out.println("Method called");
    }
    
}
