package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public interface IResource {
    
    String snippet();
    String title();
}
