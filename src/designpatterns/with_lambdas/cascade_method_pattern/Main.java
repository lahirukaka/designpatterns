package designpatterns.with_lambdas.cascade_method_pattern;

/**
 *
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        Mailer.send(mailer->
            mailer.setAddresses("from@gmail.com", "to@gmail.com")
                .setBody("Sample Body !!!")
                .setSubject("This is a test")
        );
    }
}
