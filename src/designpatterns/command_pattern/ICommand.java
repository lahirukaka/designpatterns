package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public interface ICommand {

    /*
    * Command should be encapsulated for Dynamism
    * Therefore no arguments shoule be passed to the execute method
    */
    void execute();
    
    void undo();
}
