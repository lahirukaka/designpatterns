package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public class Snake implements Animal{

    @Override
    public void born() {
        System.out.println("Snake Born");
    }
}
