package designpatterns.proxy_pattern;

/**
 * Proxy comes in Three intentions
 * 1. Remote Proxy - which is a interface to a remote service
 * 2. Virtual Proxy - which implements a caching mechanism
 * 3. Protection Proxy - which implements a access control mechanism
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        System.out.println("creating and call parse");
        IBookParser parser = new LazyBookParser(1, 100000000);
        parser.parse();
        System.out.println("\nask for result");
        System.out.println(parser.getResult());
        System.out.println("\nask for result again");
        System.out.println(parser.getResult());
    }
}
