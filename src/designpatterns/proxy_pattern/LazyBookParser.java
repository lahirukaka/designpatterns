package designpatterns.proxy_pattern;

/**
 * This is a Virtual proxy which do caching and lazy evaluation
 * @author lahiru
 */
public class LazyBookParser implements IBookParser{
    
    private final int min;
    private final int max;
    
    private BookParser parser=null;

    public LazyBookParser(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public long getResult() {
        //Lazy evaluation and Caching
        if(parser==null){
            parser = new BookParser(min, max);
            parser.parse();
        }
        return parser.getResult();
    }

    @Override
    public void parse() {
        //just ignore this call since the calculation is not needed in here
        //Or we can verify that calculation params are correct Or parmissions
    }
    
    public int getMinValue(){return min;}
    public int getMaxValue(){return max;}
}
