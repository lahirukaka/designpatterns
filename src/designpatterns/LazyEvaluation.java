package designpatterns;

import java.util.function.Supplier;

/**
 *
 * @author lahiru
 */
public class LazyEvaluation {

    private int calculate(int number, String when)
    {
        System.out.println("called with "+number+" "+when);
        return number;
    }
    
    public void start(int number)
    {
        int x=number;
        /*
        *This will evaluate the calculate method even it is not required in the if statement due to x is not greater than 5
        *This is called eager eveluation
        */
        int temp = calculate(number, "eagerly");
        
        /*
        *This will not evaluate calculate unless it is needed in the if method, only when number is greater than 5
        */
        Supplier<Integer> temp2 = ()->calculate(number, "lazyly");
        
        if(x > 5 && temp > 5 && temp2.get() > 5)
        {
            System.out.println("Result...");
        }else
        {
            System.out.println("No result...");
        }
    }
}
