package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public class Receiver {
    
    private double sum;

    public Receiver(double sum) {
        this.sum = sum;
    }
    
    public synchronized void addNumber(int number)
    {
        sum += (double)number;
        System.out.println("Result : "+sum);
    }
    
    public synchronized void substractNumber(int number)
    {
        sum -= (double)number;
        System.out.println("Result : "+sum);
    }
    
    public synchronized void multiplyByNumber(int number)
    {
        sum *= (double)number;
        System.out.println("Result : "+sum);
    }
    
    public synchronized void divideByNumber(int number)
    {
        sum /= (double)number;
        System.out.println("Result : "+sum);
    }
}
