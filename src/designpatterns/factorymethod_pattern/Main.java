package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        IFactory mammal = new MammalFactory();
        IFactory reptile = new ReptileFactory();
        
        mammal.create().born();
        mammal.create().born();
        mammal.create().born();
        reptile.create().born();
    }
}
