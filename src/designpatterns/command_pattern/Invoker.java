package designpatterns.command_pattern;

/**
 * ICommands can be defined with different Commands when initialize
 * @author lahiru
 */
public class Invoker {
    
    private final ICommand add;
    private final ICommand substract;
    private final ICommand multiply;
    private final ICommand divide;

    public Invoker(ICommand add, ICommand substract, ICommand multiply, ICommand divide) {
        this.add = add;
        this.substract = substract;
        this.multiply = multiply;
        this.divide = divide;
    }
    
    public void add()
    {
        this.add.execute();
    }
    
    public void substract()
    {
        this.substract.execute();
    }
    
    public void multiply()
    {
        this.multiply.execute();
    }
    
    public void divide()
    {
        this.divide.execute();
    }
}
