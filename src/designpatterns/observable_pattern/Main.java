package designpatterns.observable_pattern;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        MulticastStation station = new MulticastStation();
        Display display = new Display();
        station.add(display);
        station.startProducing(1, 100);
    }
}
