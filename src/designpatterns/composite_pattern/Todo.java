package designpatterns.composite_pattern;

/**
 * This is a leaf. It doesn't have any sub todos
 * @author lahiru
 */
public class Todo implements IComponent{
    
    private final String text;

    public Todo(String text) {
        this.text = text;
    }

    @Override
    public StringBuilder render() {
        StringBuilder html = new StringBuilder();
        html.append("\n<li>").append(text).append("</li>");
        return html;
    }
}
