package designpatterns.state_pattern;

/**
 *
 * @author lahiru
 */
public class ClosedGateState implements IGateState{

    @Override
    public IGateState pay() {
        //paying when door is closed does not change anything until payOk signal received.
        //we just need to process the payment
        /*process the payment*/
        return this;
    }

    @Override
    public IGateState payOk() {
        //when payOk and gate is closed, we need to open the gate and change state
        /*coding to open the gate*/
        return new OpenGateState();
    }

    @Override
    public IGateState payFailed() {
        //when payment failed we dont need to change state
        return this;
    }

    @Override
    public IGateState personEntered() {
        //this might be a false positive
        return this;
    }

    @Override
    public String toString() {
        return "Gate is Closed";
    }
    
    
}
