package designpatterns.observable_pattern;

/**
 *
 * @author lahiru
 */
public interface IObservable {
    void add(IObserver observer);
    void remove(IObserver observer);
    void notifyObservers(int value);
}
