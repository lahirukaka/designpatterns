package designpatterns.composite_pattern;

import java.util.Arrays;
import java.util.List;

/**
 * IComponent is the kind and there are many types that belong to that kind
 * But those types have different implementations of the same behavior
 * In this case render() has two different behaviors for the same kind
 * 
 * In specifically, A component can be either a single entity or composite of entries
 * 
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        List<IComponent> day1 = 
                Arrays.asList(new Todo("Goto work"), 
                    new TodoList("Meet customer", 
                        Arrays.asList(new Todo("Ask name"), 
                                    new Todo("Discuss"))), 
                    new Todo("Get lunch"),
                    new Todo("Come home"));
        StringBuilder html = new StringBuilder("<ul>");
        day1.stream()
                .forEach(component->html.append(component.render()));
        html.append("\n</ul>");
        System.out.println(html.toString());
    }
}
