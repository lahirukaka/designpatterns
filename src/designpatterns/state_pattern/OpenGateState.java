package designpatterns.state_pattern;

/**
 *
 * @author lahiru
 */
public class OpenGateState implements IGateState{

    @Override
    public IGateState pay() {
        //when gate is open and receive pay signal again
        //Gate should still be open
        // so return the OpenGateState as the new state
        return this;
    }

    @Override
    public IGateState payOk() {
        //still should be open
        return this;
    }

    @Override
    public IGateState payFailed() {
        return this;
    }

    @Override
    public IGateState personEntered() {
        //when the person entered, the open door should be closed
        //and change state to closed
        /*coding to close the door and return closed state*/
        return new ClosedGateState();
    }

    @Override
    public String toString() {
        return "Gate is Open";
    }
    
    
}
