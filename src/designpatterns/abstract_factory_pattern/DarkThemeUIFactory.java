package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class DarkThemeUIFactory implements IFactoryUI{

    @Override
    public Button createButton() {
        return new DarkButton();
    }

    @Override
    public TextView createTextView() {
        return new DarkTextView();
    }
}
