package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class LightButton extends Button{

    @Override
    public void render() {
        System.out.println("Light Button With Dark Text");
    }
}
