package designpatterns;

import java.util.function.Consumer;

/**
 *
 * @author lahiru
 */
public class ExecuteAround {

   public static void use(String param, Consumer<ExecuteAround> instance)
   {
       ExecuteAround object = new ExecuteAround(param);
       //do something before giving object to the user
       try{
            instance.accept(object);
       }finally{
           //do somthing after used by the user
           object.close();
       }
   }
    
   private ExecuteAround(String param)
   {
       System.out.println("creating with "+param);
   }
   
   public ExecuteAround method1()
   {
       System.out.println("Method 1 called");
       return this;
   }
   
   public ExecuteAround method2()
   {
       System.out.println("Method 2 called");
       return this;
   }
   
   private void close()
   {
       System.out.println("cleanup...");
   }
}
