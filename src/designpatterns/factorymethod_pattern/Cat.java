package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public class Cat implements Animal{

    @Override
    public void born() {
        System.out.println("Cat born");
    }
}
