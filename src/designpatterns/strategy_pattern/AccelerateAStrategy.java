package designpatterns.strategy_pattern;

/**
 *
 * @author lahiru
 */
public final class AccelerateAStrategy implements IAccelerateStrategy {

    @Override
    public void accelerate() {
        System.out.println("Starting A strategy to accelerate");
    }
    
    
}
