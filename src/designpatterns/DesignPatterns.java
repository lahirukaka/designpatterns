package designpatterns;

import designpatterns.with_lambdas.cascade_method_pattern.Mailer;

/**
 *
 * @author lahiru
 */
public class DesignPatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Execute Around Pattern
//        callExecuteAroundPattern();

        //Cascade Pattern
        //callCascadePattern();
        
        //Lazy Evaluation
        lazyEvaluationWithLambda();
    }
    
    public static void lazyEvaluationWithLambda()
    {
        new LazyEvaluation().start(4);
    }
    
    public static void callCascadePattern()
    {
//        Mailer.Mailer1.send(mail->{
//            mail.setAddresses("from@gmail.com", "to@gmail.com")
//                    .setSubject("This is the subject")
//                    .setBody("This is the body");
//        });
    }
    
    public static void callExecuteAroundPattern()
    {
        ExecuteAround.use("argument 1", instance->{
            instance.method1()
                    .method2();
        });
    }
    
}
