package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class DarkTextView extends TextView {

    @Override
    public void render() {
        System.out.println("Dark TextView With Light Text");
    }
}
