package designpatterns.state_pattern;

/**
 *
 * @author lahiru
 */
public interface IGateState {
    
    IGateState pay();
    IGateState payOk();
    IGateState payFailed();
    IGateState personEntered();
}
