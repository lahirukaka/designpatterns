package designpatterns.adapter_pattern;

/**
 *
 * @author lahiru
 */
public interface IAdapter {

    void call();
}
