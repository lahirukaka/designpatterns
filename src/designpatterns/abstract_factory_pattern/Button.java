package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public abstract class Button {
    
    public abstract void render();
}
