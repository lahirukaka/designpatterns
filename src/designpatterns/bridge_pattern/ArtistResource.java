package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public class ArtistResource implements IResource {

    private final Artist artist;

    public ArtistResource(Artist artist) {
        this.artist = artist;
    }
    
    @Override
    public String snippet() {
        return artist.getBio();
    }

    @Override
    public String title() {
        return artist.getName();
    }
}
