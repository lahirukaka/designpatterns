package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public class Artist {
    
    private final String name;
    private final String bio;

    public Artist(String name, String bio) {
        this.name = name;
        this.bio = bio;
    }

    public String getName() {
        return name;
    }

    public String getBio() {
        return bio;
    }
}
