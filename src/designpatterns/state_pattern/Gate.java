package designpatterns.state_pattern;

/**
 *
 * @author lahiru
 */
public class Gate {
    
    private final IGateState state;

    public Gate(IGateState state) {
        this.state = state;
    }
    
    public Gate pay()
    {
        return changeState(state.pay());
    }
    
    public Gate payOk()
    {
        return changeState(state.payOk());
        
    }
    
    public Gate payFailed()
    {
        return changeState(state.payFailed());
    }
    
    public Gate personEntered()
    {
        return changeState(state.personEntered());
    }
    
    public Gate changeState(IGateState state)
    {
        System.out.println(this.state.toString()+" > "+state.toString());
        if(state == this.state){return this;}
        return new Gate(state);
    }
}
