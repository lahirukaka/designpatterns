package designpatterns.proxy_pattern;

import java.util.stream.IntStream;

/**
 *
 * @author lahiru
 */
public class BookParser implements IBookParser{

    private final int minValue;
    private final int maxValue;
    private long result=0;

    public BookParser(int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }
    
    public final void calculateAndReturn()
    {
        System.out.println("calculating...");
        result = IntStream.range(minValue, maxValue)
                .map(i->i*i*i)
                .mapToDouble(i->i/3.412)
                .map(d->d/100)
                .mapToLong(d->Math.round(d))
                .sum();
    }

    @Override
    public void parse() {
        //may be we can spawn a thread to go async
        calculateAndReturn();
    }
    
    @Override
    public long getResult() {
        return this.result;
    }
    
}
