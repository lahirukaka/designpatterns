package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class LightTextView extends TextView{

    @Override
    public void render() {
        System.out.println("Light TextView With Dark Text");
    }
}
