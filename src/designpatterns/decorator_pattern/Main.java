package designpatterns.decorator_pattern;

/**
 * This pattern is best if methods have different behaviors rather different properties
 * @author lahiru
 */
public class Main {
 
    public static void main(String[] args)
    {
        IBeverage coffee_vanilla = new VanillaDecorator(new Coffee());
        IBeverage coffee_choc = new ChocolateDecorator(new Coffee());
        IBeverage milk_choc = new ChocolateDecorator(new Milk());
        IBeverage milk_vanilla = new VanillaDecorator(new Milk());
        IBeverage milk_vanilla_choc = new ChocolateDecorator(new VanillaDecorator(new Milk()));
        IBeverage milk_double_choc = new ChocolateDecorator(new ChocolateDecorator(new Milk()));
        
        System.out.println(coffee_vanilla.description()+" cost is "+coffee_vanilla.cost());
        System.out.println(coffee_choc.description()+" cost is "+coffee_choc.cost());
        System.out.println(milk_choc.description()+" cost is "+milk_choc.cost());
        System.out.println(milk_vanilla.description()+" cost is "+milk_vanilla.cost());
        System.out.println(milk_vanilla_choc.description()+" cost is "+milk_vanilla_choc.cost());
        System.out.println(milk_double_choc.description()+" cost is "+milk_double_choc.cost());
    }
}
