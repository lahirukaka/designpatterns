package designpatterns.strategy_pattern;

/**
 *
 * @author lahiru
 */
public class Vehicle {
    
    private final IAccelerateStrategy iAccelerateStrategy;

    public Vehicle(IAccelerateStrategy iAccelerateStrategy) {
        this.iAccelerateStrategy = iAccelerateStrategy;
    }
    
    protected void accelerate()
    {
        iAccelerateStrategy.accelerate();
    }
}
