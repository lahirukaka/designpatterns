package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class DarkButton extends Button {

    @Override
    public void render() {
        System.out.println("Dark Button with White Text");
    }
}
