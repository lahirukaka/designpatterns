package designpatterns.command_pattern;

/**
 *
 * @author lahiru
 */
public class Substract implements ICommand{

    private final Receiver receiver;
    private final int number;

    public Substract(Receiver receiver, int number) {
        this.receiver = receiver;
        this.number = number;
    }
    
    @Override
    public void execute() {
        receiver.substractNumber(number);
    }
    
    @Override
    public void undo() {
        receiver.addNumber(number);
    }
}
