package designpatterns.observable_pattern;

/**
 *
 * @author lahiru
 */
public interface IObserver {
    void update(int value);
}
