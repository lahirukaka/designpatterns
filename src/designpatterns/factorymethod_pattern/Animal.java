package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public interface Animal {
    
    void born();
}
