package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public class Coffee implements IBeverage{

    
    
    @Override
    public int cost() {
        return 1;
    }

    @Override
    public String description() {
        return "Coffee";
    }
    
    
}
