package designpatterns.abstract_factory_pattern;

/**
 *
 * @author lahiru
 */
public class LightThemeUIFactory implements IFactoryUI{

    @Override
    public Button createButton() {
        return new LightButton();
    }

    @Override
    public TextView createTextView() {
        return new LightTextView();
    }
}
