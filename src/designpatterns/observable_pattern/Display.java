package designpatterns.observable_pattern;

/**
 *
 * @author lahiru
 */
public class Display implements IObserver{

    @Override
    public void update(int value) {
        System.out.println("received : "+value);
    }
}
