package designpatterns.with_lambdas.execute_around_pattern;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        Resource.use(resource -> {
            resource.operation1()
                    .operation2();
        });
    }
}
