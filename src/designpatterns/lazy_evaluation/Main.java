package designpatterns.lazy_evaluation;

/**
 *
 * @author lahiru
 */
public class Main {

    public static void main(String[] args)
    {
        final int x=14;
        final Lazy<Integer> temp = new Lazy(()->compute(2));
        
        if(x<5 && temp.get()>7) System.out.println("result 1");
        else System.out.println("result 2");
    }
    
    public static int compute(int num)
    {
        System.out.println("called compute");
        return num*2;
    }
}
