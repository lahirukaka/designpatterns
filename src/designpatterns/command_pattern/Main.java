package designpatterns.command_pattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        Receiver receiver = new Receiver(0);
        
        List<ICommand> commands = Arrays.asList(
                new Add(3, receiver),
                new Substract(receiver, 2),
                new Multiply(receiver, 7),
                new Divide(receiver, 3)
        );
        
        Invoker invoker = new Invoker(
                commands.get(0),
                commands.get(1),
                commands.get(2),
                commands.get(3)
        );
        
        invoker.add();
        invoker.substract();
        invoker.multiply();
        invoker.divide();
        
        //undoing commands
        Collections.reverse(commands);
        commands.stream()
                .forEach(command->command.undo());
    }
}
