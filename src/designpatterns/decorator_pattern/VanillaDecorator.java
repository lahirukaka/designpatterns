package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public class VanillaDecorator extends AddonFlavorsDecorator{

    public VanillaDecorator(IBeverage beverage) {
        this.beverage = beverage;
    }
    
    @Override
    public int cost() {
        return beverage.cost()+3;
    }

    @Override
    public String description() {
        return beverage.description()+" + Vanilla";
    }
    
    
}
