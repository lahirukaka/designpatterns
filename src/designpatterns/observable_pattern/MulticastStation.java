package designpatterns.observable_pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 *
 * @author lahiru
 */
public class MulticastStation implements IObservable{

    private final List<IObserver> observers;

    public MulticastStation() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void add(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public void remove(IObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(int value) {
        observers.stream()
                .forEach(obs->obs.update(value));
    }
    
    public void startProducing(int min, int max)
    {
        IntStream.rangeClosed(min, max)
                .map(i->i*i)
                .forEach(this::notifyObservers);
        removeAllObservers();
    }
    
    private void removeAllObservers()
    {
        observers.clear();
    }
}
