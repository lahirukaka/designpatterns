package designpatterns.factorymethod_pattern;

/**
 *
 * @author lahiru
 */
public class Dog implements Animal{

    @Override
    public void born() {
        System.out.println("Dog born ");
    }
}
