package designpatterns.proxy_pattern;

/**
 *
 * @author lahiru
 */
public interface IBookParser {
    
    long getResult();
    void parse();
}
