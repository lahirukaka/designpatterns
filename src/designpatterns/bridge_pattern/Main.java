package designpatterns.bridge_pattern;

/**
 * View and Resource are not coupled. We use composition instead of coupling.
 * No need to create Concrete classes that duplicate code
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        Book aBook = new Book("World History", "D.B Ravo");
        Artist artist = new Artist("Eminem", "This is the bio of Eminem");
        
        LargeView large_book_view = new LargeView(new BookResource(aBook));
        LargeView large_artist_view = new LargeView(new ArtistResource(artist));
        SmallView small_book_view = new SmallView(new BookResource(aBook));
        SmallView small_artist_view = new SmallView(new ArtistResource(artist));
        
        System.out.println(large_book_view.render());
        System.out.println(large_artist_view.render());
        System.out.println(small_book_view.render());
        System.out.println(small_artist_view.render());
    }
}
