package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public class ChocolateDecorator extends AddonFlavorsDecorator{

    public ChocolateDecorator(IBeverage beverage) {
        this.beverage = beverage;
    }
    
    @Override
    public int cost() {
        return beverage.cost()+2;
    }

    @Override
    public String description() {
        return beverage.description()+" + Chocolate";
    }
    
    
}
