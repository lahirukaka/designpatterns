package designpatterns.decorator_pattern;

/**
 *
 * @author lahiru
 */
public class Milk implements IBeverage{

    @Override
    public int cost() {
        return 2;
    }

    @Override
    public String description() {
        return "Milk";
    }
    
    
}
