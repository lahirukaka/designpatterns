package designpatterns.state_pattern;

/**
 * Instead of conditional statements, Polymorphism is used in this state machine
 * @author lahiru
 */
public class Main {
    
    public static void main(String[] args)
    {
        Gate gate = new Gate(new ClosedGateState());
        
        gate = gate.personEntered();
        gate = gate.pay();
        gate = gate.payOk();
        gate = gate.personEntered();
        gate = gate.pay();
        gate = gate.payOk();
        gate = gate.pay();
        gate = gate.personEntered();
    }
}
