package designpatterns.bridge_pattern;

/**
 *
 * @author lahiru
 */
public class LargeView extends View {

    public LargeView(IResource resource) {
        super(resource);
    }
    
    @Override
    public String render() {
        return this.resource.title() +"\n"+
                this.resource.snippet()+"\n\n";
    }
}
