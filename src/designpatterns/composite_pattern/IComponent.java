package designpatterns.composite_pattern;

/**
 *
 * @author lahiru
 */
public interface IComponent {
    
    StringBuilder render();
}
